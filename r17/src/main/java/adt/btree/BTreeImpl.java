package adt.btree;

import java.util.ArrayList;

public class BTreeImpl<T extends Comparable<T>> implements BTree<T> {

   protected BNode<T> root;
   protected int order;

   public BTreeImpl(int order) {
      this.order = order;
      this.root = new BNode<T>(order);
   }

   @Override
   public BNode<T> getRoot() {
      return this.root;
   }

   @Override
   public boolean isEmpty() {
      return this.root.isEmpty();
   }

   @Override
   public int height() {
      return height(this.root);
   }

   protected int height(BNode<T> node) {
      int ret = 0;
      if (node.isEmpty())
         ret = -1;
      else if (!node.isLeaf())
         ret = 1 + height(node.getChildren().get(0));

      return ret;
   }

   @Override
   public BNode<T>[] depthLeftOrder() {
      ArrayList<BNode<T>> array = new ArrayList<BNode<T>>();
      this.depthLeftOrder(root, array);

      return (BNode<T>[]) array.toArray(new BNode[0]);
   }

   protected void depthLeftOrder(BNode<T> node, ArrayList<BNode<T>> array) {

      array.add(node);

      if (!node.isLeaf())
         for (int i = node.getOrder() - 1; i >= 0; i--)
            if (node.getChildren().get(i) != null && !node.getChildren().get(i).isEmpty())
               depthLeftOrder(node.getChildren().get(i), array);
   }

   @Override
   public int size() {
      return this.size(root);
   }

   protected int size(BNode<T> node) {
      int tam = 0;

      tam += node.getElements().size();

      if (!node.isLeaf())
         for (BNode<T> child : node.getChildren())
            tam += size(child);

      return tam;
   }

   @Override
   public BNodePosition<T> search(T element) {
      return this.search(root, element);
   }

   @Override
   public void insert(T element) {
      BNode<T> node = findInsertionNode(this.root, element);
      node.addElement(element);

      while (root.parent != null && !root.parent.isEmpty())
         root = root.parent;
   }

   protected BNode<T> findInsertionNode(BNode<T> node, T element) {
      BNode<T> ret = node;
      if (!node.isLeaf()) {
         int i = 0;
         while (i < this.order && element.compareTo(node.getElementAt(i)) < 0)
            i++;

         ret = findInsertionNode(node.getChildren().get(i), element);
      }

      return ret;
   }

   protected BNodePosition<T> search(BNode<T> node, T element) {
      BNodePosition<T> ret = null;

      int i = 0;
      while (i < this.order && element.compareTo(node.getElementAt(i)) < 0)
         i++;

      if (node.getElementAt(i).compareTo(element) == 0)
         ret = new BNodePosition<>(node.getChildren().get(i), i);
      else if (!node.isLeaf())
         ret = search(node.getChildren().get(i), element);

      return ret;
   }

   protected void split(BNode<T> node) {
      // TODO Implement your code here
      throw new UnsupportedOperationException("Not Implemented yet!");
   }

   protected void promote(BNode<T> node) {
      // TODO Implement your code here
      throw new UnsupportedOperationException("Not Implemented yet!");
   }

   // NAO PRECISA IMPLEMENTAR OS METODOS ABAIXO
   @Override
   public BNode<T> maximum(BNode<T> node) {
      // NAO PRECISA IMPLEMENTAR
      throw new UnsupportedOperationException("Not Implemented yet!");
   }

   @Override
   public BNode<T> minimum(BNode<T> node) {
      // NAO PRECISA IMPLEMENTAR
      throw new UnsupportedOperationException("Not Implemented yet!");
   }

   @Override
   public void remove(T element) {
      // NAO PRECISA IMPLEMENTAR
      throw new UnsupportedOperationException("Not Implemented yet!");
   }

}
