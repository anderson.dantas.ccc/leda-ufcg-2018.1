package sorting.simpleSorting;

import sorting.AbstractSorting;
import util.Util;

/**
 * The selection sort algorithm chooses the smallest element from the array and
 * puts it in the first position. Then chooses the second smallest element and
 * stores it in the second position, and so on until the array is sorted.
 */
public class SelectionSort<T extends Comparable<T>> extends AbstractSorting<T> {

	@Override
	public void sort(T[] array, int leftIndex, int rightIndex) {
		if(leftIndex >= 0 && rightIndex < array.length) {
			for(int i = 0; i < rightIndex - leftIndex; i++) {
				int index = leftIndex + i;
				for(int j = index + 1; j <= rightIndex; j++) {
					if(array[j].compareTo(array[index]) == -1)
						index = j;
				}
				Util.swap(array, index, leftIndex + i);
			}
		}
	}
}
