package sorting.simpleSorting;

import sorting.AbstractSorting;
import util.Util;

/**
 * As the insertion sort algorithm iterates over the array, it makes the
 * assumption that the visited positions are already sorted in ascending order,
 * which means it only needs to find the right position for the current element
 * and insert it there.
 */
public class InsertionSort<T extends Comparable<T>> extends AbstractSorting<T> {

	@Override
	public void sort(T[] array, int leftIndex, int rightIndex) {
		if(leftIndex >= 0 && rightIndex < array.length) {
			for(int i = 1; i < rightIndex - leftIndex + 1; i++) {
				for(int j = leftIndex + i; j > leftIndex && array[j].compareTo(array[j-1]) == -1; j--)
					Util.swap(array, j, j-1);
			}
		}
	}
}
