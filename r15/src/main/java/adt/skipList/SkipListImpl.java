package adt.skipList;

public class SkipListImpl<T> implements SkipList<T> {

	protected SkipListNode<T> root;
	protected SkipListNode<T> NIL;

	protected int maxHeight;

	protected double PROBABILITY = 0.5;

	public SkipListImpl(int maxHeight) {
		this.maxHeight = maxHeight;
		root = new SkipListNode(Integer.MIN_VALUE, maxHeight, null);
		NIL = new SkipListNode(Integer.MAX_VALUE, maxHeight, null);
		connectRootToNil();
	}

	/**
	 * Faz a ligacao inicial entre os apontadores forward do ROOT e o NIL Caso
	 * esteja-se usando o level do ROOT igual ao maxLevel esse metodo deve
	 * conectar todos os forward. Senao o ROOT eh inicializado com level=1 e o
	 * metodo deve conectar apenas o forward[0].
	 */
	private void connectRootToNil() {
		for (int i = 0; i < maxHeight; i++) {
			root.forward[i] = NIL;
		}
	}

	
	@Override
	public void insert(int key, T newValue, int height) {
		SkipListNode<T>[] update = new SkipListNode[maxHeight];
		SkipListNode<T> aux = root;
		for(int i = maxHeight-1; i >= 0; i--) {
			while(aux.forward[i].getKey() < key)
				aux = aux.forward[i];
			update[i] = aux;
		}
		
		aux = aux.forward[0];
		if(aux.getKey() == key) {
			aux.setValue(newValue);
		} else {
			SkipListNode<T> newNode = new SkipListNode<T>(key, height, newValue);
			for(int i = height-1; i >= 0; i--) {
				newNode.forward[i] = update[i].forward[i];
				update[i].forward[i] = newNode;
			}
		}
	}

	@Override
	public void remove(int key) {
		SkipListNode<T>[] update = new SkipListNode[maxHeight];
		SkipListNode<T> aux = root;
		for(int i = maxHeight-1; i >= 0; i--) {
			while(aux.forward[i].getKey() < key)
				aux = aux.forward[i];
			update[i] = aux;
		}
		
		aux = aux.forward[0];
		
		if(aux.getKey() == key)
			for(int i = aux.height()-1; i >= 0 && update[i].forward[i].getKey() == key; i--)
				update[i].forward[i] = aux.forward[i];
	}

	@Override
	public int height() {
		int h = 0;
		SkipListNode<T> aux = root;
		while(!aux.forward[0].equals(NIL)) {
			h = Math.max(h, aux.forward[0].height());
			aux = aux.forward[0];
		}
		return h;
	}

	@Override
	public SkipListNode<T> search(int key) {
		SkipListNode<T> aux = root;
		SkipListNode<T> ret = null;
		for(int i = maxHeight-1; i >= 0; i--)
			while(aux.forward[i].getKey() < key)
				aux = aux.forward[i];
		
		aux = aux.forward[0];
		
		if(aux.getKey() == key)
			ret = aux;
		
		return ret;
	}

	@Override
	public int size() {
		int tam = 0;
		SkipListNode<T> aux = root;
		while(!aux.forward[0].equals(NIL)) {
			aux = aux.forward[0];
			tam++;
		}
		return tam;
	}

	@Override
	public SkipListNode<T>[] toArray() {
		// +2 for sintinels
		int elementsSize = size() + 2;
		SkipListNode<T>[] retArray = new SkipListNode[elementsSize]; 
		SkipListNode<T> aux = root;
		for(int i = 0; i < elementsSize; i++) {
			retArray[i] = aux;
			if(i + 1 < elementsSize)
				aux = aux.forward[0];
		}
		
		return retArray;
	}

}
