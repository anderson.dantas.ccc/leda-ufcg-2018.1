package adt.bst.extended;

import java.util.Comparator;

import adt.bst.BSTImpl;
import adt.bst.BSTNode;

/**
 * Implementacao de SortComparatorBST, uma BST que usa um comparator interno em
 * suas funcionalidades e possui um metodo de ordenar um array dado como
 * parametro, retornando o resultado do percurso desejado que produz o array
 * ordenado.
 * 
 * @author Adalberto
 *
 * @param <T>
 */
public class SortComparatorBSTImpl<T extends Comparable<T>> extends BSTImpl<T> implements SortComparatorBST<T> {

	private Comparator<T> comparator;

	public SortComparatorBSTImpl(Comparator<T> comparator) {
		super();
		this.comparator = comparator;
	}

	@Override
	public T[] sort(T[] array) {
		while (!super.isEmpty())
			super.remove(super.getRoot().getData());

		for (T element : array)
			this.insert(element);

		return super.order();
	}

	@Override
	public T[] reverseOrder() {
		int listSize = size();
		T[] values = (T[]) new Comparable[listSize];

		if (!root.isEmpty())
			reverseOrderArray(values, root, 0);

		return (T[]) values;
	}

	public Comparator<T> getComparator() {
		return comparator;
	}

	public void setComparator(Comparator<T> comparator) {
		this.comparator = comparator;
	}

	@Override
	protected BSTNode<T> search(BSTNode<T> node, T element) {
		BSTNode<T> ret = new BSTNode<T>();

		if (!node.isEmpty()) {
			if (node.getData().equals(element)) {

				ret = node;
			} else if (comparator.compare(node.getData(), element) < 0) {
				if (node.getRight() instanceof BSTNode)
					ret = search((BSTNode<T>) node.getRight(), element);
			} else {

				if (node.getLeft() instanceof BSTNode)
					ret = search((BSTNode<T>) node.getLeft(), element);
			}
		}

		return ret;
	}

	@Override
	protected void insert(BSTNode<T> node, BSTNode<T> parent, T element) {
		if (node.isEmpty()) {
			node.setParent(parent);
			node.setData(element);
			node.setRight(new BSTNode<T>());
			node.setLeft(new BSTNode<T>());
		} else {

			if (comparator.compare(node.getData(), element) < 0) {

				if (node.getRight() instanceof BSTNode)
					insert((BSTNode<T>) node.getRight(), node, element);
			} else {

				if (node.getLeft() instanceof BSTNode)
					insert((BSTNode<T>) node.getLeft(), node, element);
			}
		}
	}

	private int reverseOrderArray(Object[] arr, BSTNode<T> node, int index) {
		if (!node.getRight().isEmpty() && node.getRight() instanceof BSTNode)
			index = reverseOrderArray(arr, (BSTNode<T>) node.getRight(), index);

		arr[index++] = node.getData();

		if (!node.getLeft().isEmpty() && node.getLeft() instanceof BSTNode)
			index = reverseOrderArray(arr, (BSTNode<T>) node.getLeft(), index);

		return index;
	}
}
