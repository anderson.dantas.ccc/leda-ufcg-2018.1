package adt.stack;

import adt.linkedList.DoubleLinkedList;
import adt.linkedList.RecursiveDoubleLinkedListImpl;

public class StackRecursiveDoubleLinkedListImpl<T> implements Stack<T> {

	protected DoubleLinkedList<T> top;
	protected int size;

	public StackRecursiveDoubleLinkedListImpl(int size) {
		this.size = size;
		this.top = new RecursiveDoubleLinkedListImpl<T>();
	}

	@Override
	public void push(T element) throws StackOverflowException {
		if(this.top.size() == this.size)
			throw new StackOverflowException();
		this.top.insertFirst(element);
		this.size++;
	}

	@Override
	public T pop() throws StackUnderflowException {
		if(this.size == 0)
			throw new StackUnderflowException();
		T front = ((RecursiveDoubleLinkedListImpl<T>) this.top).getData();
		this.top.removeFirst();
		this.size--;
		return front;
	}

	@Override
	public T top() {
		return ((RecursiveDoubleLinkedListImpl<T>) this.top).getData();
	}

	@Override
	public boolean isEmpty() {
		return this.top.size() == 0;
	}

	@Override
	public boolean isFull() {
		return this.top.size() == this.size;
	}

}
