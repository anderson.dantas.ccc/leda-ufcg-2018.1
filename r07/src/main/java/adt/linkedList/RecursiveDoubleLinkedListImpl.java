package adt.linkedList;

public class RecursiveDoubleLinkedListImpl<T> extends
		RecursiveSingleLinkedListImpl<T> implements DoubleLinkedList<T> {

	protected RecursiveDoubleLinkedListImpl<T> previous;

	public RecursiveDoubleLinkedListImpl() {

	}

	public RecursiveDoubleLinkedListImpl(T data,
			RecursiveSingleLinkedListImpl<T> next,
			RecursiveDoubleLinkedListImpl<T> previous) {
		super(data, next);
		this.previous = previous;
	}

	@Override
	public void insertFirst(T element) {
		if(super.isEmpty()) {
			this.data = element;
			this.next = new RecursiveDoubleLinkedListImpl<>();
		} else {
			RecursiveDoubleLinkedListImpl<T> newNext = new RecursiveDoubleLinkedListImpl<T>(this.data, this.next, this);
			this.data = element;
			this.next = newNext;
		}
	}

	@Override
	public void removeFirst() {
		if(!this.isEmpty()) {
			this.data = this.next.data;
			this.next = this.next.next;
			
			if(this.next == null)
				this.next = new RecursiveDoubleLinkedListImpl<T>(null, null, this);
			else
				this.next = new RecursiveDoubleLinkedListImpl<T>(this.next.data, this.next.next, this);
		}
	}

	@Override
	public void removeLast() {
		if(!this.isEmpty()) {
			if(this.next.isEmpty())
				this.data = this.next.data;
			else {
				this.next = new RecursiveDoubleLinkedListImpl<T>(this.next.data, this.next.next, this);
				((RecursiveDoubleLinkedListImpl<T>)this.next).removeLast();
			}
		}
	}

	public RecursiveDoubleLinkedListImpl<T> getPrevious() {
		return previous;
	}

	public void setPrevious(RecursiveDoubleLinkedListImpl<T> previous) {
		this.previous = previous;
	}

}
