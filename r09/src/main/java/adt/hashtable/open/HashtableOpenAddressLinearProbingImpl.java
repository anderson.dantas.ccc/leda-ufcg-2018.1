package adt.hashtable.open;

import adt.hashtable.hashfunction.HashFunctionClosedAddressMethod;
import adt.hashtable.hashfunction.HashFunctionLinearProbing;

public class HashtableOpenAddressLinearProbingImpl<T extends Storable> extends
		AbstractHashtableOpenAddress<T> {
	
	public HashtableOpenAddressLinearProbingImpl(int size,
			HashFunctionClosedAddressMethod method) {
		super(size);
		hashFunction = new HashFunctionLinearProbing<T>(size, method);
		this.initiateInternalTable(size);
	}

	@Override
	public void insert(T element) {
		if(super.table.length == super.elements)
			throw new HashtableOverflowException();
		
		int index = this.getInsertIndex(element);
		
		if(index != -1) {
			super.table[index] = element;
			super.elements++;
		}
	}

	@Override
	public void remove(T element) {
		int index = this.indexOf(element);
		
		if(index != -1) {
			super.table[index] = super.deletedElement;
			super.elements--;
		}
	}

	@Override
	public T search(T element) {
		int index = this.indexOf(element);
		
		if(index == -1)
			return null;
			
		return (T) super.table[index];
	}

	@Override
	public int indexOf(T element) {
		int probe = 0;
		int index = -1;
		if(this.hashFunction instanceof HashFunctionLinearProbing<?> && element != null) {
			index = ((HashFunctionLinearProbing<T>) this.hashFunction).hash(element, probe);
			
			while(probe < this.table.length && super.table[index] != null && !element.equals(super.table[index]))
				index = ((HashFunctionLinearProbing<T>) this.hashFunction).hash(element, ++probe);
			
			if(probe == this.table.length || super.table[index] == null)
				index = -1;
		}
		return index;
	}

	protected int getInsertIndex(T element){
		int probe = 0;
		int index = -1;
		if(this.hashFunction instanceof HashFunctionLinearProbing<?> && element != null) {
			index = ((HashFunctionLinearProbing<T>) this.hashFunction).hash(element, probe);
			
			while(probe < this.table.length 
					&& super.table[index] != null 
					&& !deletedElement.equals(super.table[index])
			) {
				index = ((HashFunctionLinearProbing<T>) this.hashFunction).hash(element, ++probe);
				super.COLLISIONS++;
			}	
			
			if(probe == this.table.length)
				index = -1;
		}
		return index;
	}
}
