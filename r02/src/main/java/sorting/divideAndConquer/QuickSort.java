package sorting.divideAndConquer;

import sorting.AbstractSorting;
import util.Util;

/**
 * Quicksort is based on the divide-and-conquer paradigm. The algorithm chooses
 * a pivot element and rearranges the elements of the interval in such a way
 * that all elements lesser than the pivot go to the left part of the array and
 * all elements greater than the pivot, go to the right part of the array. Then
 * it recursively sorts the left and the right parts. Notice that if the list
 * has length == 1, it is already sorted.
 */
public class QuickSort<T extends Comparable<T>> extends AbstractSorting<T> {

   @Override
   public void sort(T[] array, int leftIndex, int rightIndex) {
	   if(leftIndex > rightIndex) {
			int temp = leftIndex;
			leftIndex = rightIndex;
			rightIndex = temp;
		}
		
		leftIndex = Math.max(0, leftIndex);
		rightIndex = Math.min(rightIndex, array.length-1);
		
	    this.sort1(array, leftIndex, rightIndex);
   }
   
   private void sort1(T[] array, int leftIndex, int rightIndex) {
	   if (leftIndex < rightIndex) {
	         int p = partition(array, leftIndex, rightIndex);
	         this.sort1(array, leftIndex, p - 1);
	         this.sort1(array, p + 1, rightIndex);
		}
   }

   private int partition(T[] array, int leftIndex, int rightIndex) {
      int j = leftIndex;
      int i = leftIndex;
      while (j < rightIndex) {
         if (array[j].compareTo(array[rightIndex]) < 1) {
            Util.swap(array, i++, j);
         }
         j++;
      }
      Util.swap(array, i, rightIndex);
      return i;
   }
}
