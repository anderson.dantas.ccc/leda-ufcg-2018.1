package sorting.divideAndConquer;

import java.util.Arrays;

import sorting.AbstractSorting;

/**
 * Merge sort is based on the divide-and-conquer paradigm. The algorithm
 * consists of recursively dividing the unsorted list in the middle, sorting
 * each sublist, and then merging them into one single sorted list. Notice that
 * if the list has length == 1, it is already sorted.
 */
public class MergeSort<T extends Comparable<T>> extends AbstractSorting<T> {

	@Override
	public void sort(T[] array, int leftIndex, int rightIndex) {
		if(leftIndex > rightIndex) {
			int temp = leftIndex;
			leftIndex = rightIndex;
			rightIndex = temp;
		}
		
		leftIndex = Math.max(0, leftIndex);
		rightIndex = Math.min(rightIndex, array.length-1);
		
		int mid = (rightIndex + leftIndex) / 2;
		
		if(rightIndex - leftIndex < 1)
			return;
		
		sort(array, leftIndex, mid);
		sort(array, mid + 1, rightIndex);
		T[] aux = Arrays.copyOf(array, array.length);
		
		int pos = leftIndex;
		int i = leftIndex;
		int j = mid+1;
		
		while(i <= mid && j <= rightIndex) {
			if(aux[i].compareTo(aux[j]) < 1)
				array[pos++] = aux[i++];
			else
				array[pos++] = aux[j++];
		}
		while(i <= mid)
			array[pos++] = aux[i++];
		
		while(j <= rightIndex)
			array[pos++] = aux[j++];
	}
}
