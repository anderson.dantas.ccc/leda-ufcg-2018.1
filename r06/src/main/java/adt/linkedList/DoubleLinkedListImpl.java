package adt.linkedList;

public class DoubleLinkedListImpl<T> extends SingleLinkedListImpl<T> implements
		DoubleLinkedList<T> {

	protected DoubleLinkedListNode<T> last;
	protected DoubleLinkedListNode<T> head;
	
	public DoubleLinkedListImpl (){
		this.head = new DoubleLinkedListNode<T>();
		last = (DoubleLinkedListNode<T>) head;
	}

	@Override
	public void insertFirst(T element) {
		DoubleLinkedListNode<T> newHead = new DoubleLinkedListNode<T>();
		
		newHead.data = element;
		
		if(!head.isNIL()) {
			newHead.next = this.head;
			this.head.previous = newHead;
		}
		
		this.head = newHead;
	}

	@Override
	public void removeFirst() {
		if(!head.isNIL()) {
			this.head = (DoubleLinkedListNode<T>) this.head.next;
			
			if(head.isNIL())
				this.last = (DoubleLinkedListNode<T>) this.head;
		
		}	 
	}

	@Override
	public void removeLast() {
		if(!this.last.isNIL())
			this.last.previous.next = this.last.next;
	}

	public DoubleLinkedListNode<T> getLast() {
		return last;
	}

	public void setLast(DoubleLinkedListNode<T> last) {
		this.last = last;
	}

	@Override
	public void insert(T element) {
		if(head.isNIL()) {
			DoubleLinkedListNode<T> newHead = new DoubleLinkedListNode<T>();
			newHead.next = head;
			newHead.data = element;
			last = head = newHead;
		} else {
			DoubleLinkedListNode<T> auxHead = head;
			
			while(!auxHead.next.isNIL())
				auxHead = (DoubleLinkedListNode<T>) auxHead.next;
			
			DoubleLinkedListNode<T> newNode = new DoubleLinkedListNode<T>();
			
			newNode.next = auxHead.next;
			newNode.data = element;
			newNode.previous = auxHead;
			
			auxHead.next = newNode;
			last = newNode;
		}
	}

	@Override
	public void remove(T element) {
		if(!this.head.isNIL()) {
			if(this.head.data == element) {
				this.head = (DoubleLinkedListNode<T>) this.head.next;
			} else {
				DoubleLinkedListNode<T> auxHead = (DoubleLinkedListNode<T>) this.head.next;
				
				while(!auxHead.isNIL() && auxHead.data != element) {
					auxHead = (DoubleLinkedListNode<T>) auxHead.next;
				}
					
				if(!auxHead.isNIL())
					auxHead.previous.next = auxHead.next;
			}
		}
	}
	
	@Override
	public boolean isEmpty() {
		return head.isNIL();
	}

	@Override
	public int size() {
		int auxSize = 0;
		DoubleLinkedListNode<T> auxHead = head;
		
		while(!auxHead.isNIL()) {
			auxHead = (DoubleLinkedListNode<T>) auxHead.next;
			auxSize++;
		}
			
		return auxSize;
	}

	@Override
	public T search(T element) {
		if(head.isNIL())
			return null;
		DoubleLinkedListNode<T> auxHead = head;
		
		while(!auxHead.isNIL() && auxHead.data != element)
			auxHead = (DoubleLinkedListNode<T>) auxHead.next;
		
		return auxHead.data;
	}
	
	@Override
	public T[] toArray() {
		int listSize = size();
		T[] values = (T[]) new Object[listSize];
		
		DoubleLinkedListNode<T> auxHead = head;
		for(int count = 0; count < listSize; count++) {
			values[count] = auxHead.data;
			auxHead = (DoubleLinkedListNode<T>) auxHead.next;
		}
		
		return values;
	}
}
