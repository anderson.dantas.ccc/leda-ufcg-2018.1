package adt.linkedList;

public class SingleLinkedListImpl<T> implements LinkedList<T> {

   protected SingleLinkedListNode<T> head;

   public SingleLinkedListImpl() {
      this.head = new SingleLinkedListNode<T>();
   }

   @Override
   public boolean isEmpty() {
      return head.isNIL();
   }

   @Override
   public int size() {
      int auxSize = 0;
      SingleLinkedListNode<T> auxHead = head;

      while (!auxHead.isNIL()) {
         auxHead = auxHead.next;
         auxSize++;
      }

      return auxSize;
   }

   @Override
   public T search(T element) {
      if (head.isNIL())
         return null;
      SingleLinkedListNode<T> auxHead = head;

      while (!auxHead.isNIL() && auxHead.data != element)
         auxHead = auxHead.next;

      return auxHead.data;
   }

   @Override
   public void insert(T element) {
      if (head.isNIL()) {
         SingleLinkedListNode<T> newHead = new SingleLinkedListNode<T>();
         newHead.next = head;
         newHead.data = element;
         head = newHead;
      } else {
         SingleLinkedListNode<T> auxHead = head;

         while (!auxHead.next.isNIL())
            auxHead = auxHead.next;

         SingleLinkedListNode<T> newNode = new SingleLinkedListNode<T>();
         newNode.next = auxHead.next;
         newNode.data = element;
         auxHead.next = newNode;
      }
   }

   @Override
   public void remove(T element) {
      if (!head.isNIL()) {
         if (head.data == element) {
            head = head.next;
         } else {
            SingleLinkedListNode<T> auxHead = head.next;
            SingleLinkedListNode<T> previous = head;

            while (!auxHead.isNIL() && auxHead.data != element) {
               previous = auxHead;
               auxHead = auxHead.next;
            }

            if (!auxHead.isNIL())
               previous.next = auxHead.next;
         }
      }
   }

   @Override
   public T[] toArray() {
      int listSize = size();
      T[] values = (T[]) new Object[listSize];

      SingleLinkedListNode<T> auxHead = head;
      for (int count = 0; count < listSize; count++) {
         values[count] = auxHead.data;
         auxHead = auxHead.next;
      }

      return values;
   }

   public SingleLinkedListNode<T> getHead() {
      return head;
   }

   public void setHead(SingleLinkedListNode<T> head) {
      this.head = head;
   }

}
