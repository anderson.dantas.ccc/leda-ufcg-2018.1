package adt.bst;

public class BSTImpl<T extends Comparable<T>> implements BST<T> {

   protected BSTNode<T> root;

   public BSTImpl() {
      root = new BSTNode<T>();
   }

   public BSTNode<T> getRoot() {
      return this.root;
   }

   @Override
   public boolean isEmpty() {
      return root.isEmpty();
   }

   @Override
   public int height() {
      int h = -1;

      if (!this.isEmpty() && root instanceof BSTNode)
         h = height(this.root);

      return h;
   }

   @Override
   public BSTNode<T> search(T element) {
      return search(root, element);
   }

   @Override
   public void insert(T element) {
      insert(root, new BSTNode<T>(), element);
   }

   @Override
   public BSTNode<T> maximum() {
      BSTNode<T> aux;

      aux = maximum(root);

      return aux;
   }

   @Override
   public BSTNode<T> minimum() {
      BSTNode<T> aux;

      aux = minimum(root);

      return aux;
   }

   @Override
   public BSTNode<T> sucessor(T element) {
      BSTNode<T> aux = root;

      if (!aux.isEmpty())
         aux = sucessor(aux, element);

      if (aux.isEmpty())
         return null;

      return aux;
   }

   @Override
   public BSTNode<T> predecessor(T element) {
      BSTNode<T> aux = root;

      if (!aux.isEmpty())
         aux = predecessor(aux, element);

      if (aux.isEmpty())
         return null;

      return aux;
   }

   @Override
   public void remove(T element) {
//	   System.out.println(element);
	   if(!root.isEmpty())
		   remove(search(element), element);
   }

   @Override
   public T[] preOrder() {
      int listSize = size();
      T[] values = (T[]) new Comparable[listSize];

      if (!root.isEmpty())
         preOrderArray(values, root, 0);

      return (T[]) values;
   }

   @SuppressWarnings("unchecked")
   @Override
   public T[] order() {
      int listSize = size();
      T[] values = (T[]) new Comparable[listSize];

      if (!root.isEmpty())
         orderArray(values, root, 0);

      return (T[]) values;
   }

   @Override
   public T[] postOrder() {
      int listSize = size();
      T[] values = (T[]) new Comparable[listSize];

      if (!root.isEmpty())
         postOrderArray(values, root, 0);

      return (T[]) values;
   }

   /**
    * This method is already implemented using recursion. You must understand
    * how it work and use similar idea with the other methods.
    */
   @Override
   public int size() {
      return size(root);
   }

   private int size(BSTNode<T> node) {
      int result = 0;
      // base case means doing nothing (return 0)
      if (!node.isEmpty()) { // indusctive case
         result = 1 + size((BSTNode<T>) node.getLeft()) + size((BSTNode<T>) node.getRight());
      }
      return result;
   }

   private int height(BSTNode<T> node) {
      int childs_max_height = -1;

      if (!node.isEmpty()) {

         if (node.getLeft() instanceof BSTNode)
            childs_max_height = 1 + height((BSTNode<T>) node.getLeft());

         if (node.getRight() instanceof BSTNode)
            childs_max_height = Math.max(childs_max_height, 1 + height((BSTNode<T>) node.getRight()));
      }

      return childs_max_height;
   }

   private BSTNode<T> search(BSTNode<T> node, T element) {
      BSTNode<T> ret = new BSTNode<T>();

      if (!node.isEmpty()) {
         if (node.getData().equals(element)) {

            ret = node;
         } else if (node.getData().compareTo(element) < 0) {
            if (node.getRight() instanceof BSTNode)
               ret = search((BSTNode<T>) node.getRight(), element);
         } else {

            if (node.getLeft() instanceof BSTNode)
               ret = search((BSTNode<T>) node.getLeft(), element);
         }
      }

      return ret;
   }

   private void insert(BSTNode<T> node, BSTNode<T> parent, T element) {
      if (node.isEmpty()) {
         node.setParent(parent);
         node.setData(element);
         node.setRight(new BSTNode<T>());
         node.setLeft(new BSTNode<T>());
      } else {

         if (node.getData().compareTo(element) < 0) {

            if (node.getRight() instanceof BSTNode)
               insert((BSTNode<T>) node.getRight(), node, element);
         } else {

            if (node.getLeft() instanceof BSTNode)
               insert((BSTNode<T>) node.getLeft(), node, element);
         }
      }
   }

   private int orderArray(Object[] arr, BSTNode<T> node, int index) {
      if (!node.getLeft().isEmpty() && node.getLeft() instanceof BSTNode)
         index = orderArray(arr, (BSTNode<T>) node.getLeft(), index);

      arr[index++] = node.getData();

      if (!node.getRight().isEmpty() && node.getRight() instanceof BSTNode)
         index = orderArray(arr, (BSTNode<T>) node.getRight(), index);

      return index;
   }

   private int postOrderArray(Object[] arr, BSTNode<T> node, int index) {
      if (!node.getLeft().isEmpty() && node.getLeft() instanceof BSTNode)
         index = postOrderArray(arr, (BSTNode<T>) node.getLeft(), index);

      if (!node.getRight().isEmpty() && node.getRight() instanceof BSTNode)
         index = postOrderArray(arr, (BSTNode<T>) node.getRight(), index);

      arr[index++] = node.getData();

      return index;
   }

   private int preOrderArray(Object[] arr, BSTNode<T> node, int index) {
      arr[index++] = node.getData();

      if (!node.getLeft().isEmpty() && node.getLeft() instanceof BSTNode)
         index = preOrderArray(arr, (BSTNode<T>) node.getLeft(), index);

      if (!node.getRight().isEmpty() && node.getRight() instanceof BSTNode)
         index = preOrderArray(arr, (BSTNode<T>) node.getRight(), index);

      return index;
   }

   private BSTNode<T> sucessor(BSTNode<T> node, T element) {
      BSTNode<T> ret = new BSTNode<T>();

      node = search(node, element);

      if (!node.isEmpty() && node.getData().equals(element)) {
         if (!node.getRight().isEmpty() && node.getRight() instanceof BSTNode) {

            ret = minimum((BSTNode<T>) node.getRight());
         } else {

            while (node instanceof BSTNode && !node.getParent().isEmpty()
                  && !node.getParent().getRight().isEmpty() && node.getParent().getRight().getData().equals(node.getData()))
               node = (BSTNode<T>) node.getParent();

            if (node instanceof BSTNode)
               ret = (BSTNode<T>) node.getParent();
         }

      }
      return ret;
   }

   private BSTNode<T> predecessor(BSTNode<T> node, T element) {
      BSTNode<T> ret = new BSTNode<T>();

      node = search(node, element);

      if (!node.isEmpty() && node.getData().equals(element)) {
         if (!node.getLeft().isEmpty() && node.getLeft() instanceof BSTNode) {

            ret = maximum((BSTNode<T>) node.getLeft());
         } else {

            while (node instanceof BSTNode && !node.getParent().isEmpty()
                  && !node.getParent().getLeft().isEmpty() && node.getParent().getLeft().getData().equals(node.getData()))
               node = (BSTNode<T>) node.getParent();

            if (node instanceof BSTNode)
               ret = (BSTNode<T>) node.getParent();
         }

      }
      return ret;
   }

   private BSTNode<T> minimum(BSTNode<T> node) {
      if (!node.isEmpty())
         while (!node.getLeft().isEmpty() && node.getLeft() instanceof BSTNode)
            node = (BSTNode<T>) node.getLeft();

      if (node.isEmpty())
         node = null;

      return node;
   }

   private BSTNode<T> maximum(BSTNode<T> node) {
      if (!node.isEmpty())
         while (!node.getRight().isEmpty() && node.getRight() instanceof BSTNode)
            node = (BSTNode<T>) node.getRight();

      if (node.isEmpty())
         node = null;

      return node;
   }
   
   private void remove(BSTNode<T> node, T element) {
	   if(!node.isEmpty()) {
		   if(node.isLeaf())
			   node.setData(null);
		   else if(!node.getLeft().isEmpty() && !node.getRight().isEmpty() && sucessor(element) != null) {
			   BSTNode<T> sus = sucessor(element);
//			   System.out.println(sus);
			   node.setData(sus.getData());
			   remove(sus, sus.getData());
			   
		   } else if(!node.getLeft().isEmpty()) {
			   
			   T value = node.getLeft().getData();
			   node.setData(value);
			   if(node.getLeft() instanceof BSTNode)
				   remove((BSTNode<T>) node.getLeft(), value);
		   } else {
			   
			   T value = node.getRight().getData();
			   node.setData(value);
			   if(node.getRight() instanceof BSTNode)
				   remove((BSTNode<T>) node.getRight(), value);
		   }
	   }
   }
}
