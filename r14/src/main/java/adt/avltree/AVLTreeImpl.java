package adt.avltree;

import adt.bst.BSTImpl;
import adt.bst.BSTNode;
import adt.bt.Util;

/**
 * 
 * Performs consistency validations within a AVL Tree instance
 * 
 * @author Claudio Campelo
 *
 * @param <T>
 */
public class AVLTreeImpl<T extends Comparable<T>> extends BSTImpl<T> implements AVLTree<T> {

   protected int height(BSTNode<T> node) {
      if (node.isEmpty())
         return 0;
      return 1 + Math.max(height((BSTNode<T>) node.getLeft()), height((BSTNode<T>) node.getRight()));
   }

   protected int calculateBalance(BSTNode<T> node) {
      int fb = -1;
      if (!node.isEmpty())
         fb = height((BSTNode<T>) node.getRight()) - height((BSTNode<T>) node.getLeft());
      return fb;
   }

   protected void rebalance(BSTNode<T> node) {
      int bf = calculateBalance(node);
      if (bf < -1) {
         if (calculateBalance((BSTNode<T>) node.getLeft()) <= 0) {
            Util.rightRotation(node);
         } else {
            Util.leftRotation((BSTNode<T>) node.getLeft());
            Util.rightRotation(node);
         }
      } else if (bf > 1) {
         if (calculateBalance((BSTNode<T>) node.getRight()) >= 0) {
            Util.leftRotation(node);
         } else {
            Util.rightRotation((BSTNode<T>) node.getRight());
            Util.leftRotation(node);
         }
      }
   }

   // AUXILIARY
   protected void rebalanceUp(BSTNode<T> node) {
      BSTNode<T> parent = (BSTNode<T>) node.getParent();
      while (parent != null && !parent.isEmpty()) {
         this.rebalance(parent);
         parent = (BSTNode<T>) parent.getParent();
      }
   }

   @Override
   public void insert(T element) {
      insert(root, new BSTNode<T>(), element);
   }

   private void insert(BSTNode<T> node, BSTNode<T> parent, T element) {
      if (node.isEmpty()) {
         node.setParent(parent);
         node.setData(element);
         node.setRight(new BSTNode<T>());
         node.setLeft(new BSTNode<T>());
      } else {

         if (node.getData().compareTo(element) < 0) {

            if (node.getRight() instanceof BSTNode)
               insert((BSTNode<T>) node.getRight(), node, element);
         } else {

            if (node.getLeft() instanceof BSTNode)
               insert((BSTNode<T>) node.getLeft(), node, element);
         }

         rebalance(node);
      }
   }

   @Override
   public void remove(T element) {
      if (!root.isEmpty())
         remove(search(element), element);
   }

   private void remove(BSTNode<T> node, T element) {
      if (!node.isEmpty()) {
         if (node.isLeaf()) {
            node.setData(null);
            rebalanceUp(node);
         } else if (!node.getLeft().isEmpty() && !node.getRight().isEmpty() && sucessor(element) != null) {
            BSTNode<T> sus = sucessor(element);

            node.setData(sus.getData());
            remove(sus, sus.getData());

         } else if (!node.getLeft().isEmpty()) {

            T value = node.getLeft().getData();
            node.setData(value);
            if (node.getLeft() instanceof BSTNode)
               remove((BSTNode<T>) node.getLeft(), value);

         } else {

            T value = node.getRight().getData();
            node.setData(value);
            if (node.getRight() instanceof BSTNode)
               remove((BSTNode<T>) node.getRight(), value);

         }
      }
   }
}
