package adt.avltree;

import java.util.ArrayList;
import java.util.Arrays;

import adt.bst.BSTNode;

public class AVLCountAndFillImpl<T extends Comparable<T>> extends AVLTreeImpl<T> implements AVLCountAndFill<T> {

   private int LLcounter;
   private int LRcounter;
   private int RRcounter;
   private int RLcounter;

   public AVLCountAndFillImpl() {

   }

   @Override
   public int LLcount() {
      return LLcounter;
   }

   @Override
   public int LRcount() {
      return LRcounter;
   }

   @Override
   public int RRcount() {
      return RRcounter;
   }

   @Override
   public int RLcount() {
      return RLcounter;
   }

   @Override
   public void fillWithoutRebalance(T[] array) {
      ArrayList<T> elementsList = new ArrayList<T>();

      for (T element : array)
         elementsList.add(element);
      for (T element : this.order())
         elementsList.add(element);

      T[] elementsToInsert = (T[]) elementsList.toArray(new Comparable[0]);

      super.root = new BSTNode<T>();

      Arrays.sort(elementsToInsert);
      fillWithoutRebalance(array, 0, elementsToInsert.length - 1);

      System.out.println(Arrays.toString(this.postOrder()));
   }

   @Override
   protected void rebalance(BSTNode<T> node) {
      int bf = calculateBalance(node);
      if (bf < -1)
         if (calculateBalance((BSTNode<T>) node.getLeft()) <= 0)
            LLcounter++;
         else
            LRcounter++;
      else if (bf > 1)
         if (calculateBalance((BSTNode<T>) node.getRight()) >= 0)
            RRcounter++;
         else
            RLcounter++;
      super.rebalance(node);
   }

   protected void fillWithoutRebalance(T[] array, int l, int r) {
      if (l <= r) {
         int mid = (l + r + 1) / 2;

         fillWithoutRebalance(array, l, mid - 1);
         fillWithoutRebalance(array, mid + 1, r);

         this.insert(array[mid]);
      }
   }
}
